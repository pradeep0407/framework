package com.evs.flipkart.testcases;

import java.io.IOException;

import javax.xml.xpath.XPath;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.evs.flipkart.util.WebUtil;

public class Test1221ProductSearch {
	
//	@FindBy(xpath="//input[@title='Search for products, brands and more']")
//	public WebElement search;
//	
//	@FindBy(xpath="//button[@class='vh79eN']")
//	public WebElement searchclik;
	
	
	
	public PriceFilter searchText() throws IOException {
		
		String mi=WebUtil.Settingprop.getProperty("search.home");
		System.out.println(mi);
	    WebUtil.sendKeys("search_mobile_mi", mi);
		WebUtil.clickOnText("search_button_click");
		WebUtil.scroll(0, 5);
		PriceFilter  ftt=PageFactory.initElements(WebUtil.driver, PriceFilter.class);
	    return	ftt;
	}
}
