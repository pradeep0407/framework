package com.evs.flipkart.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.evs.flipkart.util.WebUtil;

public class RegistrationPage {

	@FindBy(xpath="//a[@class='_2AkmmA _1LctnI jUwFiZ']//span")
	public WebElement signup;
	
	@FindBy(xpath="//input[@class='_2zrpKA']")
	public WebElement mobile;
	
	@FindBy(xpath="//input[@autocomplete='on']")
	public WebElement otp;
	
	@FindBy(xpath="//input[@type='password']")
	public WebElement pass;
	
	@FindBy(xpath="//button[@type='submit']")
	public WebElement submit;
	
	public void registerMob(){
		
	    String Mob=WebUtil.Settingprop.getProperty("mobile.number");
	    String ota=WebUtil.Settingprop.getProperty("ot.p");
	    String pas=WebUtil.Settingprop.getProperty("pass.word");
//		WebUtil.click(signup);
//		mobile.sendKeys(Mob);
//		otp.sendKeys(ota);
//		pass.sendKeys(pas);
//		WebUtil.click(submit);   it have to change by or property 
	}
	
}
