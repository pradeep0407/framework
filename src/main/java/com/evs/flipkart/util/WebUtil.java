package com.evs.flipkart.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import org.testng.annotations.Test;

import com.evs.flipkart.pages.LoginPage;

public class WebUtil {
     public static WebDriver driver;
     public static Properties Settingprop;
     public static Properties OrProperties;
     public static Map<String, String> map;
	
     public static void LaunchBrowser(){
   
	//String 	sel=JOptionPane.showInputDialog("CHOOSE BROWSER");
		try {
			loadSettings();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String browser=Settingprop.getProperty("browser.name");
	if(browser.equalsIgnoreCase("ch")){
		
	  System.setProperty("webdriver.chrome.driver", "drivers//chromedriver.exe");
	     driver = new ChromeDriver();
	     driver.manage().timeouts().implicitlyWait(500,TimeUnit.SECONDS);
	}
	else if (browser.equalsIgnoreCase("ff")){
		 System.setProperty("webdriver.gecko.driver", "drivers//geckodriver.exe");
		    driver = new FirefoxDriver();
	  }
	
	else if (browser.equalsIgnoreCase("ie")){
		 System.setProperty("webdriver.ie.driver", "andheri//drivers//geckodriver.exe");
		    driver = new InternetExplorerDriver();
	  }

	}

	public static void loadSettings() throws IOException{ 
		FileInputStream fis=new FileInputStream("setting.properties"); // setting.properties load and submit in fis variable
	    Settingprop=new Properties(); // property method for assign and this method became global;
		Settingprop.load(fis);   // file totally loaded
		
	}
	public static  Properties loadPropertiesFile() throws IOException{
		FileInputStream fis=new FileInputStream("Locators.properties"); // setting.properties load and submit in fis variable
		Properties	OrProperties=new Properties(); // property method for assign and this method became global;
		OrProperties.load(fis);  
		return OrProperties;
	}
	public static WebElement getWebelement(String propertyName) {// going to convert webelement to string
		Properties OrProperties=null;
		try {
			OrProperties = loadPropertiesFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String propertyValue=OrProperties.getProperty(propertyName);
		//got property name like username, password. all
		String[]locator=propertyValue.split("##");// this split 
		String locatorValue=locator[0]; // locator 0 means xpath expression
		String locatorType=locator[1]; // it is locator type
		WebElement we=null;        
		if(locatorType.equalsIgnoreCase("Xp")==true){
		we=driver.findElement(By.xpath(locatorValue));
		}else if(locatorType.equalsIgnoreCase("Nm")){
			we=driver.findElement(By.name(locatorValue));
		}
		return we;// taken webelement as null for match condition
	}
	
	public static List<WebElement> getListTypeWebelement(String propertyName) throws IOException{// going to convert list <webelement> to string
		Properties OrProperties =loadPropertiesFile();
		String propertyValue=OrProperties.getProperty(propertyName);
		//got property name like username, password. all
		String[]locator=propertyValue.split("##");// this split 
		String locatorValue=locator[0]; // locator 0 means xpath expression
		String locatorType=locator[1]; // it is locator type
		List <WebElement> we=null;
		if(locatorType.equalsIgnoreCase("Xp")==true){
		we=driver.findElements(By.xpath(locatorValue));
		}else if(locatorType.equalsIgnoreCase("Nm")){
			we=driver.findElements(By.name(locatorValue));
		}
		return we;// taken webelement as null for match condition
	}
	
//	public static void getLocatorsProperty() throws IOException{
//		FileInputStream    	fl=new  FileInputStream("Locators.properties");// for locator property
//		OrProperties=	new Properties();
//		OrProperties.load(fl); 
//	}
	
	public static LoginPage url(){
		String urls=Settingprop.getProperty("application.url");
		// it suppose to use in method where u want to use val corresponding to this getProperty is basic default method
		driver.get(urls);
	  LoginPage	lo=PageFactory.initElements(WebUtil.driver, LoginPage.class);
	  return lo;
	}
	
//	public static void sendText(WebElement send, String dataKeyName){
//		String   send=map.get(dataKeyName);
//		send.sendKeys(send);
//	}
	  
	public static void sendKeys(String  propertyName,String dataKeyName) throws IOException{
		WebElement we=getWebelement(propertyName);
		//String   send=map.get(dataKeyName);
		try{
			we.sendKeys(dataKeyName);
		}catch(StaleElementReferenceException e){
			 we=getWebelement(propertyName);
				we.sendKeys(dataKeyName);
		}
	}
	
	public static void clickOnText(String propertyName) throws IOException{
	WebElement    str=	getWebelement(propertyName);
	try{
		str.click();
	}catch(StaleElementReferenceException X){
		getWebelement(propertyName);
		str.click();
	}
	}
	
	
	
//	public static void click(WebElement forclick){
//		
//		forclick.click();
//	}
	
	public static void screenShot(String dynmic){
		TakesScreenshot	tc=(TakesScreenshot) driver;
	    File	src=tc.getScreenshotAs(OutputType.FILE);
	    String ts=getTimeStamp().replaceAll(",", "_").replaceAll(" ", "_").replaceAll(":", "_");
	    File folder=new File("screenshot");
	    if(folder.exists()==false){
	    	folder.mkdirs();
	    }
	   // it will put for folder test-output
	    File   dest=new File("screenshot\\"+dynmic+ts+".jpeg");
	    try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static String getTimeStamp(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
		DateFormat df=sdf.getDateTimeInstance();
		Date dtObj=new Date();
		return df.format(dtObj);
	}
	
	public static void closeWindow(){
		driver.close();
	}

	public static void excelRead(String testCaseIdArg) throws IOException{
		    map=new HashMap();        
		    //removed left side variable to define it global
	 FileInputStream fis=new FileInputStream("Book4444.xlsx");
	 //  give relative  path 
	 Workbook	excel=new XSSFWorkbook(fis);     
	 Sheet      getsheet=excel.getSheet("Sheet1");   
	 //sheet loaded
	 int           lastrow=getsheet.getLastRowNum(); 
	 // till last row 9
	 int excelreaddatafrom=-1;        
	 //-1 is use for unnessary data don't come and don't compare any other...
	 for(int i=1; i<=lastrow-1; i++){  
		 // so loop is stated 0- but i passed i=1 TC002 (loop { started)
		Row   allsheetrowdata=getsheet.getRow(i); 
		//  each data will assign in i
		Cell  allcell=allsheetrowdata.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK); 
		//it is use for cell value that will start from 0 and any cell will empty.so it will null.
		String  allval=allcell.getStringCellValue(); 
		//it is use for take value from cell.
		if(allval.equalsIgnoreCase(testCaseIdArg)){ 
			// condition for match....
			excelreaddatafrom=i; 
			// excel row data again reassign in condition with (i -- is size of row)
			break; 
			// condition matched so, passes and stop on tc005
		}  //  if condition closed
	 }   // loop closed
	 Row  excelrow=getsheet.getRow(excelreaddatafrom);
	 // taken total row size from sheet(excelreaddatafrom size of row)
	 Short     allcell=excelrow.getLastCellNum(); 
	 //  only tc005 all data stored in variable
	 for(int j=2; j<=allcell; j=j+2){    
		 // started loop username till last cell and j+2 will load admin because again loop start from 0 and go till 4cell 
		Cell  cellval=excelrow.getCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK);
		//taking cell value from FieldName1 (if any cell will be blank then missing will consider as null )
		String    vv=cellval.getStringCellValue(); 
		// this is value username
	    Cell	keyname=excelrow.getCell(j+1, MissingCellPolicy.CREATE_NULL_AS_BLANK);
	    // again i took val 3rd value because j+1 is 2+1 = admin
	    String     keyvl= keyname.getStringCellValue();
	    // i am taking value in variable
	    //System.out.println("vv"+vv+"keyvl++++"+keyvl); 
	    map.put(vv, keyvl);   
	    //used hashmap interface to store two string value
	} 
	}
	public static void scroll(int x, int y){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy("+x+","+y+")", "");
	}
	public static Set<String> handlesValue(){
			  Set<String>	driv=driver.getWindowHandles();
			return driv;
			
	}
	
	public static String searchTitle(){
	   String	title=driver.getTitle();
	  return  title;
}
	
	public static void switchtowindow(String hanlr){
		   WebDriver	title=driver.switchTo().window(hanlr);
		  
	}
	
	public static String text(WebElement txt){
		 String 	alltext=txt.getText();
		     return alltext;
}
	
	public static void Move(String  elementKeyName){
		WebElement ele=getWebelement(elementKeyName);
		Actions act= new Actions(driver);
		act.moveToElement(ele).build().perform();
}
	
	public static void dragDrop(WebElement drag,WebElement drop ){
		Actions act= new Actions(driver);
		act.dragAndDrop(drag, drop).build().perform();
		
}
	
	public static void selectBox(String propertyName,String val ) throws IOException{
	WebElement	selectelement=getWebelement(propertyName);
		Select sl=new Select(selectelement);
		sl.selectByValue(val);
	}
	
	public static void main(String[] args) throws IOException{
	
		getPageData("Sheet1", "CMM_001");
	}
	
	public static void getPageData(String sheet,String rowidpass) throws IOException{
		Map<String, String>  cmap= new HashMap();
	 Workbook	xl=new XSSFWorkbook("TestData.xlsx");
	 Sheet      sheetobj=xl.getSheet(sheet);
	 int        lastrow= sheetobj.getLastRowNum();  
	 int    number=-1;  // number variable has assign i value and -1 is use for unnessary data
	    for(int i=1; i<=lastrow-1; i++){
	    Row 	torowvalue=sheetobj.getRow(i); // got all row 
	    Cell    cellobj=torowvalue.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK); // got all row from 0
	    String  value=cellobj.getStringCellValue(); // got all row cell value
	    System.out.println(value);
	    if(value.equalsIgnoreCase(rowidpass)){  // macthed argument then break 
	    	 number= i;   //all row assign in i that is declear before loop
	    	break;
	    }
	   }  
		
	  Row  tilllastrow=sheetobj.getRow(number);  //called cmm_001 row by number   
	 Row headerRow= sheetobj.getRow(0);         //   row started from 0 SelectAccountType 
	  int lastcellsize =headerRow.getLastCellNum(); // got 0 cell value till last 
	  for(int j=2; j<=lastcellsize; j=j+1){    // lastcellsize value till last 
		  Cell keyName=headerRow.getCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK);// it is start from 0 row and 2 cell number and loop also started from 0
		  String keYNameValue=keyName.getStringCellValue();
		Cell  fieldvalue=tilllastrow.getCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK); // now loop go on 1 row by loop and 2 cell data will pick 
		String   fieldvaluen = fieldvalue.getStringCellValue();
			
		cmap.put(keYNameValue, fieldvaluen);
	  }   
	  System.out.println("");
	}
}
