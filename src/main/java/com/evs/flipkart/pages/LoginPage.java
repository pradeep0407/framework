package com.evs.flipkart.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.evs.flipkart.testcases.SliderBanner;
import com.evs.flipkart.testcases.Test1221ProductSearch;
import com.evs.flipkart.util.WebUtil;

public class LoginPage {
     
//	@FindBys(@FindBy(xpath="//input[@class='_2zrpKA']"))
//	public  List<WebElement> username;
	
//	@FindBy(xpath="//input[@class='_2zrpKA']")
//	public  WebElement username;
//	
//	@FindBy(xpath="//input[@class='_2zrpKA _3v41xv']")
//	public  WebElement password;
//	
//	@FindBy(xpath="//button[@class='_2AkmmA _1LctnI _7UHT_c']")
//	public  WebElement log;
//	
//           public  SliderBanner loginPageD(){
//         	int count=after_login_slider.size();password.pw=admin
//		for(int i=0; i<=count-1;i++){
//			username.get(i);
//        			}
	    
		public SliderBanner loginPageD() throws IOException{
		   
		   String us=WebUtil.Settingprop.getProperty("user.name");
		    String ps= WebUtil.Settingprop.getProperty("user.password");
		         
			   
				WebUtil.sendKeys("login_home_username", us);
				WebUtil.sendKeys("login_home_password", ps);
	            WebUtil.clickOnText("login_home_login");
   
          SliderBanner   hm= PageFactory.initElements(WebUtil.driver, SliderBanner.class);
        return hm;
	}

}
