package com.evs.flipkart.pages;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.evs.flipkart.testcases.FlipkartLogout;
import com.evs.flipkart.testcases.Footerlink;
import com.evs.flipkart.testcases.PriceFilter;
import com.evs.flipkart.testcases.ProductDetails;
import com.evs.flipkart.testcases.SliderBanner;
import com.evs.flipkart.testcases.Test1221ProductSearch;
import com.evs.flipkart.util.ListnerUse;
import com.evs.flipkart.util.WebUtil;

@Listeners(com.evs.flipkart.util.ListnerUse.class)	
public class Homepage extends ListnerUse{
     
	
	
	@Test
	public void website() throws IOException, InterruptedException{
       WebUtil.LaunchBrowser();
       LoginPage login = WebUtil.url();
      SliderBanner sl =login.loginPageD(); 
       // WebUtil.Move("logout_hover");
	   // WebUtil.clickOnText("click_on_logout");
      Test1221ProductSearch  searchpage=sl.sliderMove();
      Thread.sleep(1000);
      PriceFilter  pricediscount=searchpage.searchText();
      pricediscount.selectPrice();
      FlipkartLogout   flip=  pricediscount.mobileLinkClick();
      flip.logout();
      //Handle.handleval();
      //WebUtil.closeWindow();
	}
	
}
